#!/bin/bash
#TP 2  -  Federico López
# Al programa se le debe pasar por parámetro el nombre de los ficheros a buscar, máximo 3 argumentos.

clear
fich=( $@ )

#valida que se haya pasado al menos un argumento y un maximo de 3.
if [[ ${#fich[@]} -eq 0 || ${#fich[@]} -gt 3 ]] ; then

        echo "debe ingresar al menos un argumento y un máximo de 3"
        exit 1
fi

#busca el contenido del indice 0 del array en todo el disco y lo guarda en la variable 'aux'
aux=$(find / -type f -name ${fich[0]} 2>&-  ) 

#valida la variable 'aux' en busca de contenido
if [ ${aux:-null} = "null" ] ; then
	echo "el fichero ${fich[0]} no existe"
else
	echo "El fichero ${fich[0]} existe y se encuentra en la siguiente ubicación ${aux}"
	#Valida si el fichero tiene permiso de ejecucion y si lo tiene pregunta si desea ejecutarlo
	test -x ${aux}
	if [ $? -eq 0 ] ; then
		ls -l ${aux}
		read -p "Desea ejecutar el fichero  ${fich[0]} [Y/N] " val
	 		if [[ $val =~ [Yy] ]];then
			          echo "Ejecutando fichero ${fich[0]}..."
			fi
	else
		
	#si no tiene permisos de ejecucion pero somos dueños, pregunta si deseamos darle permiso de ejecucion 
		test -O ${aux}
		if [ $? -eq 0 ];then
			read -p "Desea otorgarle permisos de ejecución al fichero ${fich[0]} [Y/N] " val1
			if [[ $val1 =~ [Yy] ]];then
			chmod +x ${aux}
			#lista detalle del fichero
			ls -l ${aux}
			fi
		else
			echo "Usted no es dueño del fichero ${fich[0]}"
		fi
	fi
fi

unset aux
#valida que exista un segundo fichero a buscar dentro del array
if [ ${fich[1]:-null} = "null" ] ; then

        exit 1
fi
 
echo ""
echo ""

#busca el contenido del indice 1 del array y lo guarda en la variable 'aux'
aux=$(find / -type f -name ${fich[1]} 2>&-  )
#valida que la variable aux tenga contenido para saber si el fichero existe
if [ ${aux:-null} = "null" ] ; then
        echo "el fichero ${fich[1]} no existe"
else
        echo "El fichero ${fich[1]} existe y se encuentra en la siguiente ubicación ${aux}"
        #Valida si el fichero tiene permiso de ejecucion y si lo tiene pregunta si desea ejecutarlo
        test -x ${aux}
        if [ $? -eq 0 ] ; then
                ls -l ${aux}
                read -p "Desea ejecutar el fichero  ${fich[1]} [Y/N] " val
                        if [[ $val =~ [Yy] ]];then
                                  echo "Ejecutando fichero ${fich[1]}..."
                        fi
        else

        #si no tiene permisos de ejecucion pero somos dueños, pregunta si deseamos darle permiso de ejecucion
                test -O ${aux}
                if [ $? -eq 0 ];then
                        read -p "Desea otorgarle permisos de ejecución al fichero ${fich[1]} [Y/N] " val1
                        if [[ $val1 =~ [Yy] ]];then
                        chmod +x ${aux}
                        #lista detalle del fichero
                        ls -l ${aux}
                        fi
                else
                        echo "Usted no es dueño del fichero ${fich[1]}"
                fi
        fi
fi

unset aux
#valida el indice 2 del array en busca de contenido
if [ ${fich[2]:-null} = "null" ] ; then
        exit 1
fi

echo ""
echo ""
#busca el contenido del indice 2 del array y lo guarda en la variable 'aux'
aux=$(find / -type f -name ${fich[2]} 2>&-  )
#valida que el fichero exista
if [ ${aux:-null} = "null" ] ; then
        echo "el fichero ${fich[2]} no existe"
else
        echo "El fichero ${fich[2]} existe y se encuentra en la siguiente ubicación ${aux}"
        #Valida si el fichero tiene permiso de ejecucion y si lo tiene pregunta si desea ejecutarlo
        test -x ${aux}
        if [ $? -eq 0 ] ; then
                ls -l ${aux}
                read -p "Desea ejecutar el fichero  ${fich[2]} [Y/N] " val
                        if [[ $val =~ [Yy] ]];then
                                  echo "Ejecutando fichero ${fich[2]}..."
                        fi
        else

        #si no tiene permisos de ejecucion pero somos dueños, pregunta si deseamos darle permiso de ejecucion
                test -O ${aux}
                if [ $? -eq 0 ];then
                        read -p "Desea otorgarle permisos de ejecución al fichero ${fich[2]} [Y/N] " val1
                        if [[ $val1 =~ [Yy] ]];then
                        chmod +x ${aux}
                        #lista detalle del fichero
                        ls -l ${aux}
                        fi
                else
                        echo "Usted no es dueño del fichero ${fich[2]}"
                fi
        fi
fi

