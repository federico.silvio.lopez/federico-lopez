#!/bin/bash
args=( $@ )
#valido que ingresen al menos una opcion
if [[ $1 == "-C" ]] || [[ $1 == "-T" ]] || [[ $1 == "-p" ]] || [[ $1 == "-b" ]] || [[ $1 == "-h" ]];then

#Valido que la IP sea valida y contenga solo numeros enteros positivos
if [[ ${args[-1]} =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]];then
	#guardo en la variable $ip el array de la ip q me pasaron pero quitandole los puntos y transformandolo en espacio
	ip=($(echo ${args[-1]} | tr "." " "))
      #valido para cada posicion del array que el numero sea menor o igual a 255
      if [[ ${ip[0]} -le 255 ]] && [[ ${ip[1]} -le 255 ]] && [[ ${ip[2]} -le 255 ]] && [[ ${ip[3]} -le 255 ]];then
      echo "ip valida"
      else 
      echo "la ip ingresada no es valida, puede usar la opcion -h para ver la ayuda"
      exit 1
      fi
      #valido que el host este separado al menos por un punto  
  elif [[ ${args[-1]} =~ ^.+\..+\..+$ ]]; then
      echo "host valido"
      elif [[ ${args[-1]} =~ ^.+\..+$ ]]; then
         echo "host valido"
              #Si la unica variable ingresada es -h muestra la ayuda
 	   elif [[ ${args[-1]} =~ -h ]];then
                  echo "Este programa valida la respuesta ping contra una ip o host
                    La forma de ejecutarlo es pasar por parámetro las opciones y el host
          
                    Las opciones disponibles son:
                    -C #numero Esta opcion se utiliza para delimitar la cantidad de echo_request enviadas al host.
          
                    -T Imprime la latencia completa de usuario a usuario
          
                     -p 4 o 6 Indica si la petición se hará con IPV4 ingresando el #4 o con IPV6 Ingresando el #IPV6
          
                    -b Permite hacer ping a una dirección de broadcast
          
                    -h Muestra la ayuda
          
                     Ejemplo: -C 4 -T -p 4 www.google.com 
                     Ejemplo: -C 2 -p 6 192.168.0.1
                     Ejmplo: -h"
else
#si no es una ip o host valido muestra el error y sale del programa
echo " Ingrese una ip del tipo x.x.x.x o un host del tipo www.google.com o google.com
       Si quiere ver la ayuda del programa ingrese la opcion -h"
exit 1
fi
for opcion in "${!args[@]}"; do
		case ${args[$opcion]} in
			-C) cantidad=${args[$(($opcion+1))]}
    				#valido que el valor sea un numero entero positivo
				if [[ $cantidad =~ ^[1-9]+[0-9]*$ ]];then
    				counter="-c $cantidad"
    				else
    				echo "Debe ingresar un numero entero positivo, puede ingresar la opcion -h para ver la ayuda"
    				exit 1
    				fi
    			;;
			-T) timestamp="-D"
			;;
			-p) proto=${args[$(($opcion+1))]}
   				#valido que el protocolo sea 4 o 6
				if [ $proto -eq 4 -o $proto -eq 6 ];then
    					p="-$proto"
   				else
   					echo "Las opciones permitidas del protocolo debe ser 4 o 6, puede ingresar la opcion -h para ver la ayuda"
   					exit 1
   			        fi
			;;
		       -b) b="-b"
			;;
                    [0-9])
	        	;;
              ${args[-1]})
		        ;;
	        	*) echo "debe ingresar al menos una opcion valida -C ; -T ; -p ; -b
				 
Tambien puede consultar la ayuda ingresando la opcion -h"
			exit 1
			;;
	       esac
done
ping $b $timestamp $p $counter ${args[-1]}

exit 0
else
	echo "Debe ingresar al menos una opción, tambien puede consultar la ayuda ingresando la opcion -h"
exit 1
fi
