#!/bin/bash
clear

#Declaro funcion para validar IP_HOST ingresado
func_valida_ip_host () {
#Valido que la IP sea valida y contenga solo numeros enteros positivos
echo ""
read -p "Por favor Ingrese dirección IP o Hostname: " in
if [[ ${in} =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]];then
        #guardo en la variable local $ip el array de la ip q me pasaron pero quitandole los puntos y transformandolo en espacio
        local ip=($(echo ${in} | tr "." " "))
      #valido para cada posicion del array que el numero sea menor o igual a 255
      if [[ ${ip[0]} -le 255 ]] && [[ ${ip[1]} -le 255 ]] && [[ ${ip[2]} -le 255 ]] && [[ ${ip[3]} -le 255 ]];then
              echo -e "\e[1;32m IP Válida!  \e[0m"
              return 0
      else
              #Si la IP no es válida exit status 1
              return 1
      fi
      #valido que el nombre del host no contenga caracteres especiales
        elif [[ ${in} =~ ^([a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.)*[a-zA-Z]{2,}$ ]]; then
                echo -e "\e[1;32m Host Válido! \e[0m"
		return 0
        else
                #Si el nombre del host exit status 1
               return 1
fi
}

#Declaro funcion que realiza backup

func_backup (){
#empiezo a cargar mi arhivo de script.
echo "#!/bin/bash" >>/$HOME/${4}
#Recorro array en busca de lista de directorios ingresados
for i in "${!args[@]}";do
#Convierto caracter / en _ para definir nombre de archivo de backup a partir del segundo caracter, esto me sirve por si se ingresa
#un path con varios subdirectorios
local name=($(echo ${args[i]:1} | tr "/" "_"))
#Realizo tar y comprimo cada directorio con la fecha del día
tar czf ${name}.$(date +%Y-%m-%d).tar.gz ${args[i]} 2>/dev/null
#Agrego linea al archivo de script en caso que luego quiera agregar la tarea al crontab
echo "tar czf ${name}.\$(date +%Y-%m-%d).tar.gz ${args[i]}" >> /$HOME/${4}
#Realizo la sincronizacion del directorio comprimido al directorio remoto, y luego elimina el archivo en el origen.
rsync -a --remove-source-files ${name}.$(date +%Y-%m-%d).tar.gz ${3}@${1}:${2}
#Agrego linea a mi script en caso que luegar quiera agregar la tarea al crontab
echo "rsync -av --remove-source-files ${name}.\$(date +%Y-%m-%d).tar.gz ${3}@${1}:${2}" >> /$HOME/${4}

done
#Le doy permiso de ejecución al script en caso que lo quiera utilizar en el crontab
chmod +x $HOME/${4}
}

#Declaro funcion para crar par de llaves publica y privada
func_keys () {
#Pasar como parametro usuario y hostname
local user=$1
local hostname=$2
#Ejecuto comando para generar par de llaves
ssh-keygen -t rsa
#ejecuto comando para copiar al host remoto la llave publica
ssh-copy-id -i ~/.ssh/id_rsa.pub ${user}@${hostname}
}


#Declaro funcion para agregar tarea al crontab
func_cron () {
#Ingreso informacion necesaria para el cron
	read -p " Ingrese minuto: " minuto
	read -p " Ingrese hora: " hora
	read -p " Ingrese dia del Mes: " dia_M
	read -p " Ingrese mes: " mes
	read -p " Ingrese dia de la semana: " dia_S
#Realizo backup del crontab
	crontab -l > cron_bkp
#Agrego linea al archivo cron_bkp
	echo " $minuto $hora $dia_M $mes $dia_S $HOME/${1} >> /var/log/${1}.log 2>> ${1}_err.log" >> cron_bkp
#Cargo el archivo en el crontab del usuario que esta ejecutando el programa
	crontab -u $USER cron_bkp
#Valido que la tarea se haya ejecutado correctamente
	if [ $? -eq 0 ];then
	      echo -e "\e[1;32m Se agrego la tarea Correctamente \e[0m"
        fi
	      rm cron_bkp


}

#Declaro funcion para validar si los directorios ingresados para realizar backup existen.

func_valida_dir () {
#Comienzo validando que a la variable args se le agregue contenido
var=1
while [ $var -ne 0 ]; do
echo "Por favor, ingrese path absolutos de directorios a realizar backup: "
#Cargo lista de directorios en la variable args
read -a  args
if [ -n "$args" ];then
        var=0
else
        echo -e "\e[5;31m  Debe ingresar al menos un valor \e[0m "
fi
done
#Deseteo la variable var
unset var

#Recorro variable
for i in "${!args[@]}";do
        dir=(${args[i]})
#Si el directorio no existe devuelve un codigo diferente a 0
	if [ ! -d ${dir} ];then
           return 1 
        fi
   done
}

###############------------------------COMIENZO DE PROGRAMA-----------------------###################

#Itero la funcion "func_valida_dir" en caso que ingrese un directorio que no exista, vuelve a solicitar ingresar nuevamente la lista
#de directorios hasta que todos los directorios ingresados existan.
var=1
while [ $var -ne 0 ]; do
func_valida_dir 
#valido el 'exit status' de la funcion, si no es igual a 0 vuelve a solicitar ingresar directorios
var=$?
        if [ $var -ne 0 ];then
                echo -e "\e[5;31m El directorio ${dir} no ha sido encontrado, vuelva a ingresar lista de directorios... \e[0m"
        else
		echo -e "\e[1;32m Directorios Válidos! \e[0m"
	fi
done

#Deseteo la variable var
unset var

#Solicito ingresar nombre de archivo en el cual se guardará el script para luego ser agregado al cron
echo ""
var=1
while [ $var -ne 0 ]; do
echo "Por favor, ingrese un nombre de archivo para guardar el script: "
read -a arch
        if [ -n "$arch" ];then
        var=0
else
        echo -e "\e[5;31m  Debe ingresar un nombre de archivo... \e[0m"
fi
done

#Deseteo la variable var
unset var
echo ""
#Solicito ingresar directorio remoto al cual se enviarán los backups y los guardo en una variable
var=1
while [ $var -ne 0 ]; do
echo "Por favor, ingrese el nombre del directorio remoto en el cual guardará el backup.. "
read -a remoto
        if [ -n "$remoto" ];then
        var=0
else
        echo -e "\e[5;31m  Debe ingresar un directorio... \e[0m"
fi
done

#Deseteo la variable var
unset var


#Itero la funcion 'func_valida_ip_host' hasta que se ingrese una ip o hostname valido, en caso que no sean validos, vuelve a solicitarlos.
var=1
while [ $var -ne 0 ]; do
func_valida_ip_host 
#valido el 'exit status' de la funcion, si no es igual a 0 vuelve a solicitar ingresar ip o hostname válidos.
var=$?
        if [ $var -ne 0 ];then
                echo -e "\e[5;31m El hostname o IP ingresado no son validos, por favor reintente... \e[0m"
        fi
done
#Deseteo variable var
unset var


#Una vez cargadas las variables necesarias, ejecuto menu de opciones del programa.

PS3="Elige tu opción: "
echo ""
#Declaro opciones del Menu
opciones=("Generar par de llaves" "Ejecutar Script de backup" "Agregar script al crontab" "salir")
select opt in "${opciones[@]}"
do
#Declaro un case con cada una de estas opciones y llamo a las funciones declaradas anteriormente
    	case $opt in

        "Generar par de llaves") echo -e "\e[0;32m Comenzando a generar par de llaves \e[0m "
               func_keys $USER $in
               ;;

        "Ejecutar Script de backup") echo -e "\e[0;32m Ejecutando backup...aguarde por favor... \e[0m "
               func_backup $in $remoto $USER $arch
	       ;;

        "Agregar script al crontab") echo -e "\e[0;32m Por favor elija horario y dia en que correra el script \e[0m "
               func_cron $arch
	      ;;

        "salir") break 2
        ;;
        *)  echo -e "\e[5;31m Opción no válida... \e[0m"
    esac
done
